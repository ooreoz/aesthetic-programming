//image variables
let laptopScreen;
let cardInformation;
let logo;
let chopperImage;
let peelerImage;
let bottleImage;
let windowImage;
let iPhoneScamImage;
let anonymousImage;
let notificationImage;

//DOM-element variables
let inputName;
let inputPostalCode;
let inputPhoneNumber;
let inputEmail;
let purchaseButton;
let stopButton;

//for capturing and using the user's data
let fullName;
let postalCode;
let phoneNumber;
let email;

//window outlines
let windowOutlineOne;
let windowOutlineTwo;
let windowOutlineThree;

//other variables
let numberOfWindowsOpen = 0;
let termsAndConditionsBoxTextSize = 6; //Size of terms and conditions text is 8, so it only has to be changed here, if we wish to increase or decrease the size of the text

//for green text in window 3
let hackingTextToShow;
let characterIndex = 0;
let showText = true;

//preloading all the images
function preload() {
  laptopScreen = loadImage('laptop_screen.png');
  cardInformation = loadImage('card_information.jpeg');
  logo = loadImage('logo.png');
  chopperImage = loadImage('chopper.png');
  peelerImage = loadImage('peeler.png');
  bottleImage = loadImage('bottles.png');
  windowImage = loadImage('window.png');
  iPhoneScamImage = loadImage('iphonescamimage.png');
  anonymousImage = loadImage('anonymous.png');
  notificationImage = loadImage('notification.png');
}

function setup() {
  //canvas
  createCanvas(1400, 750);
  background('white');
  image(laptopScreen, 0, 0, 1400, 750);
  textWrap(WORD);

  //background of screen
  push();
  noStroke();
  fill(184, 186, 207);
  rect(182, 58, 1039, 577);
  pop();

  //logo
  push();
  noStroke();
  fill(255, 165, 82)
  circle(250, 120, 100);
  image(logo, 200, 90, 100, 70);
  pop();

  //shopping bag
  push();
  fill(31, 74, 117);
  noStroke();
  textSize(30)
  text("Your bag", 260, 210)
  pop();

  //the big rectangle containing the bag
  push();
  stroke(31, 74, 117);
  fill(230, 233, 241)
  rect(250, 235, 400, 300)
  pop();

  //lines seperating the different items in the bag
  push();
  stroke(31, 74, 117);
  line(250, 320, 650, 320);
  line(250, 405, 650, 405)
  line(250, 490, 650, 490)
  pop();

  //images/product descriptions
  image(chopperImage, 260, 245, 65, 65);
  text("9-in-1 vegetable chopper and slicer", 350, 265, 150);
  text("1", 525, 283);
  text("kr. 75,82", 575, 283);

  image(peelerImage, 260, 330, 65, 65);
  text("Automatic fruit peeler", 350, 355, 150)
  text("1", 525, 368);
  text("kr. 62,30", 575, 368);

  image(bottleImage, 260, 415, 65, 65);
  text("Motivational daily water bottle with goals", 350, 435, 150)
  text("3", 525, 453);
  text("kr. 116,82", 575, 453);

  //"total" info
  push();
  textStyle(BOLD);
  text("Total", 450, 517);
  text("5", 525, 517);
  text("kr. 254,94", 575, 517);
  pop();

  //contact
  push();
  noStroke();
  fill(31, 74, 117);
  textSize(30);
  text("Your information", 760, 125)
  pop();

  //big white rectangle
  push();
  stroke(31, 74, 117)
  rect(750, 150, 400, 450)
  pop();

  //the different text-input fields with description
  push();
  textSize(15)
  noStroke();
  fill(100)
  text("Name", 775, 220)
  inputName = createInput();
  inputName.position(855, 200);
  inputName.style('font-size', '20px');
  inputName.style('color', 'dimgrey');

  text("Postcode", 775, 270)
  inputPostalCode = createInput();
  inputPostalCode.position(855, 250);
  inputPostalCode.style('font-size', '20px');
  inputPostalCode.style('color', 'dimgrey');

  text("Phone #", 775, 320)
  inputPhoneNumber = createInput();
  inputPhoneNumber.position(855, 300);
  inputPhoneNumber.style('font-size', '20px');
  inputPhoneNumber.style('color', 'dimgrey');

  text("E-mail", 775, 370)
  inputEmail = createInput();
  inputEmail.position(855, 350);
  inputEmail.style('font-size', '20px');
  inputEmail.style('color', 'dimgrey');

  pop();
  //card-info image
  image(cardInformation, 780, 410, 350, 140)

  //purchase button
  push();
  purchaseButton = createButton("Purchase");
  purchaseButton.position(910, 550);
  purchaseButton.style('font-size', '15px');
  purchaseButton.style('background-color', 'rgb(255, 165, 82)');
  purchaseButton.mousePressed(showWindows);
  pop();

  //terms and conditions box
  push();
  fill(255, 165, 82);
  noStroke();
  rect(250, 605, 300, 30);
  pop();

  //the initial (pre-click) terms and conditions box with only the title
  push();
  noStroke();
  textStyle(BOLD);
  textSize(termsAndConditionsBoxTextSize);
  text("Terms and conditions", 260, 622);
  textSize(termsAndConditionsBoxTextSize + 4);
  text("^", 530, 625);
  pop();
}

function showWindows() {
  //sets numberOfWindowsOpen to 1, to indicate that one window has been opened
  numberOfWindowsOpen = 1;

  //executes the displayNotification() function after 1 second (1000 milliseconds)
  setTimeout(displayNotification, 1000);

  //assings the window outline image, the position and size to variable windowOutlineOne
  windowOutlineOne = image(windowImage, 190, 100, 600, 500);

  //inserts iPhoneScamImage at location (232, 142) and with size (517,395)
  image(iPhoneScamImage, 232, 142, 517, 395);
}

function displayNotification() {
  //captures the value of inputName
  fullName = inputName.value();
  image(notificationImage, 960, 70, 250, 90);

  //and writes in in the notification
  push();
  fill(186);
  noStroke();
  textSize(10);
  text(fullName, 1028, 120.5);
  pop();
}

function hideElements() {
  //hides the elements when the second window appears (this function is executed when the second window is shown)
  inputName.hide();
  inputPostalCode.hide();
  inputPhoneNumber.hide();
  inputEmail.hide();
  purchaseButton.hide();
}

function mousePressed() {
  //if the mouse is pressed within window one and numberOfWindowsOpen is 1 (the first is open) the second window appears
  if (mouseX > 230 && mouseX < 750 && mouseY > 125 && mouseY < 533 && numberOfWindowsOpen === 1) {
    hideElements();
    windowOutlineTwo = image(windowImage, 700, 150, 500, 500);
    image(anonymousImage, 735, 192, 430, 395);

    numberOfWindowsOpen = 2; //two windows are now open
    //executes some of draw
  }

  //the same as the above if-statement, but 
  if (mouseX > 740 && mouseX < 1160 && mouseY > 175 && mouseY < 583 && numberOfWindowsOpen === 2) {
    windowOutlineThree = image(windowImage, 400, 250, 450, 350);

    numberOfWindowsOpen = 3; //three windows are now open
    //executes some of draw again
  }

  if (mouseX > 250 && mouseX < 550 && mouseY > 605 && mouseY < 635) { //if the terms and conditions box is pressed, it executes the function (expands and adds text)
    termsAndConditionsBoxExpanded();
  }
}

function draw() {
  //assigns the value (input) of the input fields to the given variables (and prints it in the console for bug-fixing)
  fullName = inputName.value();
  postalCode = inputPostalCode.value();
  phoneNumber = inputPhoneNumber.value();
  email = inputEmail.value();

  console.log(fullName);
  console.log(phoneNumber);
  console.log(postalCode);
  console.log(email);

  //variables are used in this text - assigned locally instead of globally - in order to use the values of the input fields: if it was assigned globally, the variables had no values (undefined)
  hackingTextToShow = "struct group_info init_groups = { .usage = ATOMIC_INIT(2) }; struct group_info *groups_alloc(int gidsetsize){ struct group_info *group_info; int nblocks; ID" + " ----//---- " + fullName + " ----//---- " + "int i; nblocks = (gidsetsize + NGROUPS_PER_BLOCK - 1) / NGROUPS_PER_BLOCK; LOCATION:" + " ----//---- " + postalCode + " ----//---- " + "nblocks = nblocks ? : 1; group_info = kmalloc(sizeof(*group_info) + nblocks*sizeof(gid_t *), GFP_USER); if (!group_info) return NULL; PHONE_NUMBER:" + " ----//---- " + phoneNumber + " ----//---- " + "group_info->ngroups = gidsetsize; group_info->nblocks = nblocks; atomic_set(&group_info->usage, 1); if (gidsetsize <= NGROUPS_SMALL) group_info->blocks[0] = group_info->small_block; else { for (i = 0; i < nblocks; i++) { gid_t *b; b = (void *)__get_free_page(GFP_USER); if (!b) goto out_undo_partial_alloc; EMAIL_ADDRESS:" + " ----//---- " + email + " ----//---- " + "group_info->blocks[i] = b; } } return group_info; out_undo_partial_alloc: while (--i >= 0) { free_page((unsigned long)group_info->blocks[i]); } READ TERMS AND CONDITIONS BELOW";

  if (numberOfWindowsOpen == 2) { //executed once two windows are open
    let arrayOfInformation = [fullName, postalCode, phoneNumber];

    for (let i = 0; i < arrayOfInformation.length; i++) {
      push();
      noStroke();
      fill('red');
      text(arrayOfInformation[i], random(800, 1000), random(205, 580));
      pop();
    }
  }

  if (numberOfWindowsOpen == 3) { //executed once three windows are open
    //draws the black background
    push();
    noStroke();
    fill(0);
    rect(432, 280, 386, 275);
    pop();

    //formats the text
    push();
    noStroke();
    fill(0, 255, 0);

    //characterIndex increases for as long as the value is shorter than the length of hackingTextToShow
    if (characterIndex < hackingTextToShow.length) {
      characterIndex++;
    }

    //for as long as the above is executed, it prints the hackingTextToShow through a substring from 0 (first character) to the value of characterIndex (so it looks like it prints one more character per frame)
    if (showText) {
      text(hackingTextToShow.substring(0, characterIndex), 435, 280, 375);
    } else { //when the stop button is pressed, showText becomes false
      textSize(20);
      text("remember to read the small print", 490, 400, 375);
    }

    pop();
  }
}

//enlarges the orange box and prints longer text
function termsAndConditionsBoxExpanded() {
  push();
  fill(255, 165, 82);
  noStroke();
  rect(250, 560, 300, 75);
  pop();

  push();
  textStyle(BOLD);
  textSize(termsAndConditionsBoxTextSize);
  text("Terms and conditions", 260, 575);
  textStyle(NORMAL)
  text("Welcome to our online store. By shopping with us, you agree to the following terms and conditions: Ordering and Payment: All prices include VAT. Payment is made via secure payment methods as displayed at checkout. By agreeing to the terms and conditions, you agree to us using and distributing your information for profit. You are giving us full access to anything we might be able to enter with your information and the rights to your digital footprint. Contact: For questions or assistance, you may not contact our customer service via email nor phone. If you wish to opt out of us using and distributing your information for profit, push the button in the lower right corner of the screen.", 260, 578, 290);

  //creates the stop-button
  stopButton = createButton("STOP");
  stopButton.position(1170, 615);
  stopButton.style('font-size', '10px');
  pop();

  //event-listener for the stopButton
  stopButton.mousePressed(stop);
}

function stop() {
  //stops any loop currently executing in draw()
  noLoop();

  //stops the generation of green text in window 3
  showText = false;

  //clears the canvas
  clear();

  //hides the DOM elements in case they are not already hidden with window 2
  hideElements();

  //creates basic canvas again
  createCanvas(1400, 750);
  background('white');
  image(laptopScreen, 0, 0, 1400, 750);

  //keeps the outline of window 3 along with the black rectangle
  windowOutlineThree = image(windowImage, 400, 250, 450, 350);
  push();
  noStroke();
  fill(0);
  rect(432, 280, 386, 275);
  pop();

  //prints the final text
  push();
  noStroke();
  fill(0, 255, 0);
  textSize(20);
  text("remember to read the small print", 490, 400, 375);
  pop();
}