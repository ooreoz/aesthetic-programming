
function setup() {
  createCanvas(800, 800);

  }
 

function draw() {
  background('#FFFFFF');

  // emoji - goth subculture

  fill(0, 0, 0);
  rect(160, 194, 80, 50); //back of hair
 
  stroke(2);
  strokeWeight(1);
  fill(165, 126, 110);
  ellipse(200, 200, 80, 80); //head

  arc(200, 280, 70, 70, PI, TWO_PI, PIE); // body
  

  strokeWeight(0);
  fill(165, 126, 110);
  rect(195, 240, 12, 12); // neck

  strokeWeight(1);
  
  fill(0, 0, 0);
  ellipse(184, 199, 26, 9); //left eye makeup
  ellipse(218, 199, 26, 9); //right eye makeup
 
  fill(255, 255, 255);
  ellipse(185, 199, 21, 15); //left eye
  ellipse(217, 199, 21, 15); //right eye

  fill(165, 126, 110);
  ellipse(200, 214, 15, 10); // nose

  fill(0, 0, 0);
  ellipse(185, 199, 10, 10); //left pupil
  ellipse(217, 199, 10, 10); //right pupil

  fill(255, 255, 255);
  ellipse(184, 198, 5, 5); // light in L pupil
  ellipse(216, 198, 5, 5); // light in R pupil
  
  noFill();
  arc(200, 222, 25, 5, 0, PI); //arched mouth
  
  fill(0, 0, 0);
  arc(200, 194, 80, 70, PI, TWO_PI, PIE);  // front of hair
  rect(232, 194, 8, 50);
  rect(160, 194, 8, 50);

  rect(166, 270, 68, 10); // shirt

  rect(195, 242, 12, 2); // choker necklace


// emoji 2

fill(0, 0, 0);
  rect(310, 194, 80, 50); //back of hair

  stroke(2);
  strokeWeight(1);
  fill(255, 206, 180);
  ellipse(350, 200, 80, 80); //head

  arc(350, 280, 70, 70, PI, TWO_PI, PIE); // body

  strokeWeight(0);
  fill(255, 206, 180);
  rect(345, 240, 12, 12); // neck

  strokeWeight(1);

  fill(0, 0, 0);
  ellipse(334, 199, 26, 9); //left eye makeup
  ellipse(368, 199, 26, 9); //right eye makeup

  fill(255, 255, 255);
  ellipse(335, 199, 21, 15); //left eye
  ellipse(367, 199, 21, 15); //right eye

  fill(255, 206, 180);
  ellipse(350, 214, 15, 10); // nose

  fill(0, 0, 0);
  ellipse(335, 199, 10, 10); //left pupil
  ellipse(367, 199, 10, 10); //right pupil

  fill(255, 255, 255);
  ellipse(334, 198, 5, 5); // light in L pupil
  ellipse(366, 198, 5, 5); // light in R pupil

  noFill();
  arc(350, 222, 25, 5, 0, PI); //arched mouth

  fill(0, 0, 0);
  arc(350, 194, 80, 70, PI, TWO_PI, PIE);  // front of hair
  rect(382, 194, 8, 50); 
  rect(310, 194, 8, 50);

  rect(316, 270, 68, 10); // shirt

  rect(345, 242, 12, 2); // choker necklace

// emoji 3

  fill(0, 0, 0);
  rect(460, 194, 80, 50); //back of hair

  stroke(2);
  strokeWeight(1);
  fill(60, 46, 40); // Changed fill color
  ellipse(500, 200, 80, 80); //head

  arc(500, 280, 70, 70, PI, TWO_PI, PIE); // body

  strokeWeight(0);
  fill(60, 46, 40); // Changed fill color
  rect(495, 240, 12, 12); // neck

  strokeWeight(1);

  fill(0, 0, 0);
  ellipse(484, 199, 26, 9); //left eye makeup
  ellipse(518, 199, 26, 9); //right eye makeup

  fill(255, 255, 255);
  ellipse(485, 199, 21, 15); //left eye
  ellipse(517, 199, 21, 15); //right eye

  fill(60, 46, 40); // Changed fill color
  ellipse(500, 214, 15, 10); // nose

  fill(0, 0, 0);
  ellipse(485, 199, 10, 10); //left pupil
  ellipse(517, 199, 10, 10); //right pupil

  fill(255, 255, 255);
  ellipse(484, 198, 5, 5); // light in L pupil
  ellipse(516, 198, 5, 5); // light in R pupil

  noFill();
  arc(500, 222, 25, 5, 0, PI); //arched mouth

  fill(0, 0, 0);
  arc(500, 194, 80, 70, PI, TWO_PI, PIE);  // front of hair
  rect(532, 194, 8, 50); 
  rect(460, 194, 8, 50); 

  rect(466, 270, 68, 10); // shirt

  rect(495, 242, 12, 2); // choker necklace

}
