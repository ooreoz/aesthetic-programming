[picture of emojis](https://gitlab.com/ooreoz/aesthetic-programming/-/blob/main/minix2/Sk%C3%A6rmbillede_2024-03-10_181600.png?ref_type=heads)
[the code](https://gitlab.com/ooreoz/aesthetic-programming/-/blob/main/minix2/minix2.html?ref_type=heads)

[runme](https://ooreoz.gitlab.io/aesthetic-programming/minix2/minix2.html?ref_type=heads)

[code](https://gitlab.com/ooreoz/aesthetic-programming/-/blob/main/minix2/minix2.js)

An emoji can have many different appearences depending on which type of emoji is being used. We have the normal emoticons with the yellow faces where their facial expressions are the most important feature used to convey a certain message. Then we have the more human looking emojis, which can also represent nonhuman beings like fairies, trolls, robots, etc., which are meant to give more representation to the user whether that is by their appearence (old, young, female, male, nonbinary) or by profession, or even religion.

However, even with all the different emojis we have today, there is still a lot of representation missing as it definetly takes time to figure out how to incorperate every single type of human into emojis. There is definetly a lack of representation for a lot of different cultures out there that use emojis or even for different types of disabilities, religions or subcultures.

When it comes to how people look, there is definetly a typical way to 'look' that doesn't necesarily consider the fact that some people aren't born looking as everyone else. People who might be born with face deformaties for example are in no way representated by these emojis. However, it's also impossible to be able to source every kind of disability and make an emoji out of it which is why the whole system that we're working with at the moment (us waiting for these developers to put out new and improved emojis) might not be the right way to improve on some of these more niche problems.

However, it's not just about the basic structure of the emojis. While that might be hard to encorperate into all the existing emojis that exist, we sitll have a lot more that we CAN actually add to the existing emojis.

In my own code, I've decided to focus on subcultures which are not really seen in emoji depictions. More specifically, I've chosen to take a look at the goth subculture, which is one of the most well known and popular subcultures that doesn't have any kind of emoji representation.

Usually, you can definetly express different emotions or aesthetics with the emojis by using a bunch of them, like for example, it would be easy to emulate what the goth subculture could look like with emoji combinations that relate to them like for example this: 🖤🦇🕷️⚰️🥀

However, we are lacking accessibility and simplicity in this. Who has time to go hunting for all these emojis when wanting to express themselves? This is why I thought that a great addition would be one of the normal human emojis, but altered to look more goth in order to give easy access to the community.

We as humans tend to want the easy way out, which is why emojis exist in the first place, we want to quickly be able to find one thing that represents us, by sliding across a keyboard of pictures, but for many, this becomes difficult as not everyone is taken into considerations.

Focusing on subcultures, I've come to the conflusion that no subcultures are REALLY represented. There is a rockstar emoji 🧑‍🎤, however, it's more from the professional side of things as it's found amongst all the other types of career emojis.

If we wanted to view this emoji in a subculture type of way, it would probably have to represent the punk-rock movement, however, it doesn't. It has no elements of what is known in that specific community, therefore someone who identifies with the community probably wouldn't use the rockstar emoji.

The same goes for the goth emoji, you would need actual identifiable markers which point to the goth subculture for someone to consider using it. However, it shouldn't be anything too specific either as there are many different types of goths out there who might not necesarily identify with every aspect of the community.

This is why I chose to go a very simplistic route, by choosing to give my emoji simple black hair, black eyeliner, a black shirt, and a black choker.

This is seen from a female perspective but a male or nonbinary goth emoji wouldn't necessarily have to be too different, I would argue that the only change would be cutting the hair shorter. However, I also feel like the emoji I created doesn't look too female and a male goth with long hair might even choose to use it.

The black color is probably the most identifiable part of the Goth subculture, and even eyeliner is something that isn't just limited to female goths. The choker is not necesarily associated with Goth on it's own, however, it's become an identifiable mark as many Goths do like a nice choker, and someone from the outside, might even associate a choker with the Goth subculture, making the emoji accessible for people even if they don't necesarily take part in the culture.

This is a simple emoji using a simple code, as I decided to simply make use of the shapes available to me in p5, like elliipses and rectangles to make up the body face and hair. Emojis aren't very complex things, however, I was definetly limited by what I could figure out how to do as even if I wanted to add more details, like maybe to the hair or to the shirt, I couldn't figure out how to do this in a way that I knew. 

references: 
https://p5js.org/reference/#/p5/arc
