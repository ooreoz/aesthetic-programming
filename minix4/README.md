
![](minix4/Skærmbillede_2024-04-07_221038.png)

[Click to run the code](https://ooreoz.gitlab.io/aesthetic-programming/minix4/index.html
)

[Click to see the code](https://gitlab.com/ooreoz/aesthetic-programming/-/blob/main/minix4/sketch.js)


The Trap of Internet Promises

While browsing the internet, no matter which sort of site you're on, you might encounter ads. Some ads are of course there to advertise products that are to be sold, however, other some ads might are selling a different product. Your data.

There are millions of fake ads online which are made to advertise, what is often, fake products, in hopes that you as a user are led to click on them. Sometimes, these ads can look like every other ad that you regularily see, like a nice bag with a nice price, however, sometimes they can also advertise with free things, like a free phone.

We have probably all seen ads that promise you presents, perhaps because you've 'won' something according to them, or because they are simply giving something out to 'the first 100 visitors'. It's all done specifically to enntice people that might not know better, and when clicked, foreign servers might suddenly get access to the data on your device, even without you knowing.

If this happens, you're probably not going to get a real warning, pehaps you'll just get more popups that ultimately lead nowhere, however, it's possible that your computer has either caught a virus that you might not notice at first, or your data has simply been transffered to be used in neferaious databases.

The code I have created shows this exact thing, but with a twist. In a world where video survaillance is on the rise, it seems like anything and everything is out to get an image of your face, whether it's security cameras, face ID's or even AI programs where they ask you to upload a picture of your face to see how you would look as a 16th Century King.

Often times, people don't even realize that they are indeed giving their facial information away, and it seems like we might not be far from even our own device's camera capturing our face information without us knowing. There are already so many rumors of smartphone cameras spying on you through differnt apps like Instagram, and it feels like it's something more and more people have started to become more paranoid towards. This is why even newer laptops now come with inbuild camera covers!

To show exactly how dangerous this is, I have created a scenario in my code where a simple popup add, which tells you that you've won an iPhone 15, is actually a scheme to get access to your face. 

By pressing on the button to claim your free iPhone, you actually allow the webpage to access your camera, which snaps a video visual of what it is seeing, which will hopefully be your face.

After pressing the button, the webpage says 'capturing data...' for 3 seconds as it tells you that it is scanning what the camera is seeing, before showing 'transferring data...' which tells you that the data it has captured is now being transfered to another server.

As this is happening, another popup seems to appear on screen, which says 'I have changed my mind', however, when clicked, you are not saved from the fake ad, and are instead welcomed by a big error message that takes over the screen, saying 'Error: Failed to execute command, data transferring' - showing that nothing can be done about the ongoing thievery.

There is no way of knowing what will happen with your face from this point on, nor is it possible for any normal user to even track where your face is going, it is lost for ever and no longer yours. And with the spread of AI programs which can fake any picture and even video, it can be scary to know that your face can now be used for absolutely anything without your knowledge, just because a simple Ad fooled you.

Many people might dismiss the fact that they've clicked on an Ad, perhaps because it was an accident and the site was closed in 0.5 seconds, however, sometiems, once you allow something access, you can never really take back that access and things can drastically escalate without you even noticing. 
