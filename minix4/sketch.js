
let cameraPopup;
let video;
let capturingFacePopup;
let dataTransferPopup;
let changedMindPopup;
let errorPopup;
let hasCapturingFacePopupShown = false;

function setup() {
  createCanvas(windowWidth, windowHeight);
  cameraPopup = createDiv('');
  cameraPopup.class('popup hidden');
  video = createCapture(VIDEO);
  video.size(320, 240);
  video.hide();
  video.parent(cameraPopup);

  setTimeout(showNewPopup, 1000);
}

function draw() {
  background(220);
}
 
function showNewPopup() { // first popup, fake advertisment 
  const newPopup = createDiv('Click here to claim your free iPhone 15');
  newPopup.class('popup');
  newPopup.mousePressed(showCapturingFacePopup); // when pressed, show a new popup, 'capturing face data'
}

function showCapturingFacePopup() { 
  const newPopup = select('.popup');
  newPopup.class('popup hidden');
  cameraPopup.class('popup');
  video.show(); // show the camera when the first popup is clicked

  // sheck if the capturing face popup has been shown
  if (!hasCapturingFacePopupShown) {
    // create the "Capturing face data" popup
    capturingFacePopup = createDiv('Capturing face data');
    capturingFacePopup.class('capturing-face-popup');
    capturingFacePopup.position(random(windowWidth - capturingFacePopup.width), random(windowHeight - capturingFacePopup.height)); // make it appear at a random place on the screen

    // mark that the popup has shown up, so that it doesn't appear again
    hasCapturingFacePopupShown = true;

    // after 3 seconds, show the data transfer popup and hide the capturing face popup
    setTimeout(() => {
      capturingFacePopup.hide();
      showDataTransferPopup();
    }, 3000);
  }

  // create a "I have changed my mind" popup
  changedMindPopup = createDiv('I have changed my mind');
  changedMindPopup.class('changed-mind-popup');
  changedMindPopup.position(random(windowWidth - changedMindPopup.width), random(windowHeight - changedMindPopup.height)); // appears at a random place on the screen

  // create an error popup when "I have changed my mind" is clicked
  changedMindPopup.mousePressed(showErrorPopup);
}

function showErrorPopup() { // error popup takes over screen
  if (!errorPopup) {
    errorPopup = createDiv('Error: Failed to execute command, data transferring');
    errorPopup.class('error-popup');
  }
}

function showDataTransferPopup() {  // 'transferring data...' popup appears
  if (!dataTransferPopup) {
    dataTransferPopup = createDiv('');
    dataTransferPopup.class('data-popup');
    const dataText = createP('Transferring data...');
    dataText.class('data-text');
    dataText.parent(dataTransferPopup);
    dataTransferPopup.position(random(windowWidth - 200), random(windowHeight - 50)); // appears at random spot on screen
  }
}

