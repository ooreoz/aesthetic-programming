# Æstetisk programmering
### MiniX08 - Group 5
[Link to MiniX08 repository](https://gitlab.com/ooreoz/aesthetic-programming/-/tree/main/minix8)

![](minix8/minix08.png)

Run the code [here!](https://ooreoz.gitlab.io/aesthetic-programming/minix8/minix08.html)

View the code [here](minix8/minix08sketch.js) :)
<br>
<br>

**What is the program about? Which API have you used and why?**

The program is a pun-generator created in the theme of a slot machine. Every time the user clicks the “SPIN” button, the program generates a new pun. We have used a joke API from https://sv443.net/jokeapi/v2/, where we have chosen the “pun” category. We decided to only use puns and exclude the other categories such as dark and miscellaneous, as the purpose of the program was to be funny and most of those were not very suitable for a school/university setting.

We chose a joke API because it's something that everyone can relate to and it was a relatively easy API to work with. We liked the idea of an old-school machine, like a slot machine, that could provide a joke for the user. It often happens that jokes aren't funny, so just like on a real slot machine you have to be lucky to win, here you also have to spin a few times to get a good joke. So it's pure luck, just like in a real casino.
<br>
<br>

**Can you describe and reflect on your process in this miniX in terms of acquiring, processing, using, and representing data? How much do you understand this data or what do you want to know more about? How do platform providers sort the data and give you the requested data?**

We tried other similar APIs initially, however we struggled with connecting to the API correctly. It displayed the JSON data in the browser as intended, but it kept on producing errors in our code and despite attempting to debug it, we could not figure out the cause of the error. Therefore, we found this API which worked without issues.

We used this APIs website to create the API url by enabling and disabling certain categories, properties and types, where the website then created the url for us.

When executing the API url in the browser it displays as a JSON file like this:

```
{
  "error": false,
  "category": "Pun",
  "type": "single",
  "joke": "Two fish in a tank. One turns to the other and says, \"Do you know how to drive this thing?\"",
  "flags": {
    "nsfw": false,
    "religious": false,
    "political": false,
    "racist": false,
    "sexist": false,
    "explicit": false
  },
  "id": 185,
  "safe": true,
  "lang": "en"
}
```

The only part of the data we were looking for in the creation of this API was the “joke” property, as this was the part to be displayed as text (the joke itself). Therefore, it was quite easy to work with, as the text to be printed was simply `jokeData.joke`.

In the “type” property we could choose both single and two-part. We chose only “single” jokes, as the two-part jokes had different property-syntax:

```
"setup": "What did the cell say when his sister cell stepped on his foot?",
"delivery": "Mitosis.",
```

As these jokes do not include the “joke” property, they would have required a different approach to writing out the areas of text.

The .js file contains comments to almost every line of code, explaining what the lines of code do, so for more explanation I refer to the links at the top of the README file.
<br>
<br>

**What are the power relations in the chosen APIs? What is the significance of APIs in digital culture?**

This API uses no unique API-key to access its data through the code as opposed to many other APIs - especially those created by big corporations such as Google and Facebook. This point is highlighted in the chapter of Aesthetic Programming: “When it comes to Google and its operations, we can see that although it provides its API for experimentation, it only does so under restrictions.” This displays a clear power dynamic in that they make their data available, however restricts who and how much the data can be used. As this API has no need to sign up before use - with the only limit being 120 requests per minute - it does not restrict the use of the API as significantly as other API providers.

APIs increase connectivity throughout the web, connecting different websites and services with each other. It works as “a communication protocol between different parts of a computer program intended to simplify software development.” Whether it is a MitID login when purchasing on the web or syncing your university calendar with your personal calendar.
<br>
<br>

**Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time.**

- How would we create a code that enables two web APIs to collaborate in a single program?
<br>

### References
- [Aesthetic Programming, chapter 8: "Que(e)ry data", Soon & Cox](https://aesthetic-programming.net/pages/8-queery-data.html)
- [JokeAPI](https://sv443.net/jokeapi/v2/)