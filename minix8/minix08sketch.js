let jokeData;
let button;
let img;

function preload() {
  img = loadImage('slot-machine.png');
}

function setup() {
  //CREATING THE CANVAS
  createCanvas(1400, 700);

  //SETTING STANDARDS FOR DISPLAY OF RECTANGLES AND TEXT AND IMAGE
  rectMode(CENTER);
  textAlign(CENTER);
  textWrap(WORD);
  imageMode(CENTER);

  //REFRESH BUTTON AND EVENT LISTENER
  button = createButton('SPIN');
  button.position(667, 500);
  button.style('color', 'white')
  button.style('font-size', '50px');
  button.style('border-radius', '50px');
  button.style('background-color', 'RGB(230,99,82)')
  button.style('border-color', 'RGB(230,99,82)')
  button.mousePressed(refreshJoke)

  //LOADS THE JOKE FROM THE API WHEN THE REFRESHBUTTON IS PRESSED AND AT SETUP
  refreshJoke();
}

function refreshJoke() {
  let url = 'https://v2.jokeapi.dev/joke/Pun?type=single';

  //LOADS THE URL AS A JSON, STORES THE DATA OF THE JOKE IN THE 'DATA' VARIABLE AND ASSIGNS THAT DATA TO THE 'JOKEDATA' VARIABLE
  //EXECUTES THE DISPLAYJOKE() FUNCTION AT SETUP
  loadJSON(url, function (data) {
    jokeData = data;
    displayJoke();
  });
}

function displayJoke() {
  //DRAWS EVERYTHING ON THE CANVAS EVERY TIME THE BUTTON IS PRESSED TO DELETE THE PREVIOUS JOKE
  background(61, 37, 30);
  //ISOLATING STYLING FOR RECT AND ELLIPSE
  push();
  stroke(247, 204, 85);
  strokeWeight(10);
  fill(111, 84, 67);
  rect(727, 550, 587, 600);
  ellipse(727, 270, 588)
  pop();

  image(img, 700, 425, 1500, 1500);
  fill(0);
  textSize(20);
  textFont('Monaco')
  text(jokeData.joke, 727, 350, 400);
}