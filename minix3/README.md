Years of Earth

![](/minix3/Skærmbillede_2024-03-19_140757.png/)

[runme](https://ooreoz.gitlab.io/aesthetic-programming/minix3/minix3.html?ref_type=heads
)

[code](https://gitlab.com/ooreoz/aesthetic-programming/-/blob/main/minix3/minix3.js)

"the time will pass anyway" ― Earl Nightingale

When the concept of the passing of time first came up, and when we started talking about having to sit and wait for something to happen, the first thing I thought of was the quote by Earl Nightingale "_the time will pass anyway._" 

The full quote reads: "_Never give up on a dream just because of the time it will take to accomplish it. The time will pass anyway_", which is something I always seem to somehow have in the back of my mind when the question of time comes up.

The quote really speaks to me as time is truly something that we don't have any control over or even ever completely understand, however, we always seem to be fighting with it. When wanting something in life, many might think that there is no hope, as it would take too long to get there or it seems too impossible to ever reach it. But we simply need to learn that the world keeps spinning without us, which is why people always say that 'action starts now'. When speaking about the throbber, it takes that concept and translates it into a digital space where we want to accomplish something through the computer, however, we are made to wait as the computer takes the time to bring it forward. It's not something we have any control over, however, we often times get extremely impatient when these things happen. 

But we have no choice but to wait for the computer, since we had an intention with the action in the first place. And when a throbber appears, we truly have no other choice but to simply sit and wait so that we may be taken to wherever we need to be. 

That's why I decided to make my throbber as a represantation of the literal passing of time. I did this by illustrating a piece of our solar system, by placing a yellow circle for the sun in the middle, and making a green ball spin around it to show the earth making it's yearly rotations around the sun.

This is a time measurement that we all know very well, and as the throbber spins around, there is even a counter which counts each and every rotation to show the passing of time in a tangible form.

You can have the webpage open with the code for as long as you want, it will keep counting the time as the throbber will keep spinning forever, or until you close the page or reset it of course.

There is no actual endgoal for this particular throbber as it's meant to represent the time we sit and wait, which can be seen as the time that is put into a task.

The code itself is quite simple.

I started by making my throbber, which is a small green circle that sits at the middle of the page:

translate(width / 2, height / 2);


  // earth throber

  let cir = 360 / num * (frameCount % num);

  rotate(radians(cir));

  noStroke();

  fill(0, 255, 0);

  circle (150, 0, 30, 22);
  

the throbber makes a circle which it rotates in a 360 radians, the circle is green.

if you look closely, the circle has a very slight trail behind it, which i made using the function:
background(20, 20, 30); the last number in the paranthesis represents the alpha value, which makes the background more transparent making the background appear faster, thus reducing the 'trail' that the circle leaves

in the middle of my throbber, i simply placed a yellow circle which represents the sun:
fill('yellow');

  circle(width / 2, height /2, 70, 11);

i also added a slightly bigger circle on top of it, which has a lower opacity so that it looks like the sun is glowing

for the background, i also decided to add blinking stars which appear on the page at random using a for loop:

at the start of my code i wrote:
let stars = [];
and
 // creating stars

  for (let i = 0; i < 100; i++) {

    stars.push(createVector(random(width), random(height)));

  }
firstly i put a variable i which is assigned the value 0, then i set the condition for it to be less than 100 which then starts the loop, and adds 1 to I

the stars.push array determines the positions of the stars by adding the random elements to its position on the canvas 

this then adds 100 stars at random x and y coordinates 

later in the code, i also make the stars blink:

for (let i = 0; i < stars.length; i++) {

    if (random() > 0.1) {

      fill (255, 255, 0);

      ellipse(stars[i].x, stars[i].y, 2, 2);

    }

the loop created here randomly determines whether each star will blink by generating a random number between 0 and 1 using the if (random( > 0.1)) - if the number is bigger than 0.1, the code will run and draw the stars at the x and y coordinates determined in the first part, the random positions


i also made a counter for the days, which counts every single rotation that the throbber makes around the yellow circle

i first started by declaring the variable rotationCounter, which i assigned the value of 0, this is what counts the rotations

i then made the conditional statement:

  if (frameCount % num === 0) {

    rotationCounter++;

  }

the if statement here checks if the current frame count is a multiple of 'num' which is what makes the rotations of the throbber. so each time a full rotation is completed it checks true, and then increases the rotation counter by 1.

i then made a line of text at the top of the page which presents this:

function displayCounter() {
    // day counter

    fill(255);

    textSize(24);

    textAlign(LEFT);

    text("Years of Earth: " + rotationCounter, 20, 40);

this function puts together the text which reads Years of Earth and adds the rotation counter which increases with every rotation

the numbers 20, 40 are the X and Y coordinates of the text

with this code, the throbber which spins a green circle earth around a yellow sun, uses a rotation counter to count every spin which is then displayed at the top of the page


this is what ultimately shows the passing of time which we experience on earth, whether the time passes on a computer or in real life :)