
let rotationCounter = 0;
let stars = [];


function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(50);

  // creating stars
  for (let i = 0; i < 100; i++) {
    stars.push(createVector(random(width), random(height)));
  }

}

function draw() {
  background(20, 20, 40);
  drawElements();
  displayCounter();


function drawElements() {
  let num = 100; // speed of throbber
  noStroke();
  fill(255, 255, 0, [2]); // lower opacity yellow for glow effect
  circle(width / 2, height / 2, 90) // bigger than the sun circle
  fill('yellow');
  circle(width / 2, height /2, 70); // actual yellow sun

  push(); // move to center
  translate(width / 2, height / 2);

  // earth throber
  let cir = 360 / num * (frameCount % num);
  rotate(radians(cir));
  noStroke();
  fill(0, 255, 0);
  circle (150, 0, 30); // x is distance from center, so circle size

  pop();
  
  if (frameCount % num === 0) {
    rotationCounter++;
 }
}

// making the stars blink
  for (let i = 0; i < stars.length; i++) {
    if (random() > 0.1) {
      fill (255, 255, 0);
      ellipse(stars[i].x, stars[i].y, 2, 2);
    }
  }
}

  function displayCounter() {
    // day counter
    fill(255);
    textSize(24);
    textAlign(LEFT);
    text("Years of Earth: " + rotationCounter, 20, 40);
  }




