let ghost;
let speed = 4;
let jumpHeight = 20;
let gravity = 1 ;
let isJumping = false;
let tree;
let house;
let circles = [];
let score = 0;
let showCongratulations = false;

function preload() {
  ghost = loadImage('ghost.png');
  tree = loadImage('deadTree.png');
  house = loadImage('house.png');
}

function setup() {
  createCanvas(600, 200); // Made canvas wider
  ghost.pos = createVector(50, height / 1.8);

for (let i = 0; i < 20; i++) {
  circles.push({
    pos: createVector(random(width), random(-200, -100)),
    caught: false
  });
}
}

function draw() {
  background(60, 70, 96);

  image(house, 400, height -210, 150, 200);

  // Draw the tree in the background
  image(tree, 80, height - 200, 110, 160); // Adjust position and size as needed
  
  // Draw the ghost
  image(ghost, ghost.pos.x, ghost.pos.y, 50, 50);

   // Draw circles and check for collisions
   for (let i = 0; i < circles.length; i++) {
    if (!circles[i].caught) {
      fill(255);
      noStroke();
      ellipse(circles[i].pos.x, circles[i].pos.y, 20, 20);

      // Check for collision with ghost
      let d = dist(circles[i].pos.x, circles[i].pos.y, ghost.pos.x, ghost.pos.y);
      if (d < 25) { // If the ghost touches the circle
        circles[i].caught = true;
        score++;
      }
 // Update circle position
 circles[i].pos.y += 2; // Adjust falling speed

 // Respawn circle if it goes off the screen
 if (circles[i].pos.y > height + 10) {
   circles[i].pos.y = random(-200, -100);
   circles[i].pos.x = random(width);
   circles[i].caught = false;
 }
}
}
  // Display score
  textAlign(RIGHT);
  textSize(20);
  fill(255);
  text("Score: " + score, width - 20, 30);

  // Display congratulations message if score reaches 20
  if (score === 20 && !showCongratulations) {
    showCongratulations = true;
  }

  if (showCongratulations) {
    background(255); // White background
    fill(0); // Black text color
    textAlign(CENTER);
    textSize(11);
    text("You have caught all the lost souls! Congratulations!", width / 2, height / 2);
  }

  // pathway
  fill(0, 0, 0);
  rect(0, height - 40, width, 60); // pathway placed under the ghost
  if (keyIsDown(65)) { // 'a' key
    ghost.pos.x -= speed;
  }
  if (keyIsDown(68)) { // 'd' key
    ghost.pos.x += speed;
  }

  if (keyIsDown(32) && !isJumping) { // space key
    ghost.pos.y -= jumpHeight;
    isJumping = true;
  }

  if (isJumping) {
    ghost.pos.y += gravity;
    if (ghost.pos.y >= height / 2) { // ground level
      ghost.pos.y = height / 2;
      isJumping = false;
    }
  }

  // Constrain ghost within canvas
  ghost.pos.x = constrain(ghost.pos.x, 5, width - 50);
}
