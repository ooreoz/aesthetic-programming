![](minix5/sun_code.png)

[see the code](https://gitlab.com/ooreoz/aesthetic-programming/-/blob/main/minix5/minix5.html?ref_type=heads)

[run the code](https://ooreoz.gitlab.io/aesthetic-programming/minix5/minix5.html?ref_type=heads
)


the suns

When thinking of pattern generations often times you might think of something very pretty and cohesive, as there are rarely any outliers in a pattern, making it pleasing to look at. Of course, there are many possible patterns which can make one quesy looking at it with enough movement. And with enough randomness added to a pattern things can definetly get crazy.

Patterns are an endless pool of posibilities in that regard, but some people are drawn to certain kinds of patterns. For me, what I really like in a pattern is the randomness that can occur, which can be intriguing, but at the same time I still like things to be cohesive and aesthetically pleasing. 

The element of surprise can even capture your attention for longer than a stable and predicting pattern can and by adding looping motions, it can even make it hard to look away.

This is why I thought of making my pattern something that can hypnotize you in a way. While looking into the sun can be dangerous, no matter how much you wish you could get lost in it, looking into an inflating and deflating sun shaped pattern can be easier on the eyes and perhaps even relaxing.

I decided to make my pattern appear randomly on screen to give that feeling of unpredictability, while also keeping a familiar side to it to make it something you do want to look at. This is why while the pattern can appear randomly on screen whenever you reload the page, it stays in place and inflates to a certain size before deflating and starting all over again in a peaceful motion.

The pattern seen here is the repeating motion of the suns getting bigger and smaller which should hopefully capture the attention of anyone seeing it as the sun rays spread across the screen the bigger they get. 

I got inspiration for my code from this code I found online which did a similar thing, however, it was controlled differently: http://www.generative-gestaltung.de/2/sketches/?01_P/P_2_0_01

For my code, I got inspired by the shape itself and the motion of inflation which I took and made adjustments to.

The suns I have drawn on my canvas are mainly ruled by the framecount, which determines when the sun pattern should inflate and deflate. I have set my durationOfInflate and durationOfDeflate to be 300, meaning that after 300 frames, it will move onto the next stage.

i then use this and make a conditional statement which makes the pattern inflate and deflate:

 if (elapsedFrames <= durationOfInflate) {

      // inflation

      radius = map(elapsedFrames, 0, durationOfInflate, 0, maxRadius);

the inflation runs for the 300 frames, which is counted by the 'durationOfInflate' variable, that makes the radius of the sun inflate to its maximum size 'maxRadius', which was determined in the code to be: 'maxRadius = width / 2 - 10' the width of the page divided in 2, -10 

after this the conditional statement enters the 'else if' section, where the sun starts deflating for 300 frames in the same way

the pattern then gets reset and is brought back to its original state where it can start inflating again, repeating the pattern

for making the sun patterns appear randomly on the page, i used a for loop in my setup function which places 40 of them at random x and y locations to make sure that there is at least one or two that appears on the page - however, they can also be seen off the page when the inflation starts. 

to produce the actual sun pattern, i use another for loop.
i first determine a circleResolution variable which determines the minimum value of the radius (2) and the max value of the radius (80) which gives the smaller circles less points and the bigger circles more points
the 'int' function is a function used to convert the mapped value of the radius range, to a whole number

after the angles and each point of the circle is calculated using the angle variable i then draw the circle using a for loop

for this, i used cos and sin to calculate where each point of the circle should be based on the angle and the radius of the circle 
the line function then draws a line from the center to each calculated point (x, y) to draw the circle of lines which represents the sun