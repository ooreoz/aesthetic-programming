let maxRadius;
let frameAtMaxRadius;
let durationOfInflate = 300; // nr of frames for inflation
let durationOfDeflate = 300; // nr of frames for deflation
let patterns = []; // the array to store the pattern

function setup() {
  createCanvas(windowWidth, windowHeight);
  strokeCap(SQUARE);

  maxRadius = width / 2 - 10;
  frameAtMaxRadius = 0;

  // creating 40 patterns at random positions
  for (let i = 0; i < 40; i++) {
    let x = random(width);
    let y = random(height);
    patterns.push({ x: x, y: y });
  }
}

function draw() {
  background(255);

  for (let pattern of patterns) {
    translate(pattern.x, pattern.y);

    let elapsedFrames = frameCount - frameAtMaxRadius;
    let radius;

    if (elapsedFrames <= durationOfInflate) {
      // Inflation phase
      radius = map(elapsedFrames, 0, durationOfInflate, 0, maxRadius);
    } else if (elapsedFrames <= durationOfInflate + durationOfDeflate) {
      // Deflation phase
      radius = map(elapsedFrames - durationOfInflate, 0, durationOfDeflate, maxRadius, 0);
    } else {
      // Reset
      frameAtMaxRadius = frameCount;
      radius = 0;
    }

    let circleResolution = int(map(radius, 0, maxRadius, 2, 80));
    let angle = TWO_PI / circleResolution;

    // Set stroke color to orange
    stroke(255, 165, 0);

    strokeWeight(map(radius, 0, maxRadius, 5, 1));

    for (let i = 0; i <= circleResolution; i++) {
      let x = cos(angle * i) * radius;
      let y = sin(angle * i) * radius;
      line(0, 0, x, y);
    }
  }
}
