let angle = 0;
let currentColor;
let nextColor;
let lerpedColor;
let button;
let currentGradient = 0; // 0 blue-purple, 1 orange-yellow, 2 pink-white

function setup() {
  createCanvas(600, 500);

  // button
  button = createButton('Change Gradient');
  button.position(width / 2 - 20, height / 2 - 15); //button position in the middle of the circles
  button.mousePressed(changeGradient);
  button.html('🎪'); // button text
  button.style('font-size','21px'); // bigger font size
  button.style('border-radius','50%'); // turn the button round
  button.size(40, 40); // width and height

  // initial colors
    currentColor = color(0, 0, 255); // Start with blue color
    nextColor = color(148, 79, 240); // End with purple color
    lerpedColor = color(0, 0, 255); // End with blue color
  }
 

function draw() {
  background('#be9de4');

  // Color
  lerpedColor = lerpColor(currentColor, nextColor, 0.05); // 0.05 is the speed
  stroke(lerpedColor);

  strokeWeight(10);
  noFill();

  // Circle
  let circleCenterX = width / 2;
  let circleCenterY = height / 2; // Middle of height and width of canvas
  let circleRadius = 100; 
  let arcStartAngle = radians(angle); 
  let arcEndAngle = radians(angle + 180); // 180 degree arc instead of full circle
  arc(circleCenterX, circleCenterY, circleRadius * 2, circleRadius * 2, arcStartAngle, arcEndAngle);

  angle += 1 + 0.04; // Spinning speed

  // Gradual color change
  if (frameCount % 45 === 0) { // Color changes every 45 frames
    currentColor = nextColor; // Setting the current color to the next color
    nextColor = lerpedColor; // Set the next color to the interpolated color
    }
  }

function changeGradient() {

 if (currentGradient === 0) {
  // change to orange-yellow gradient
 
    currentColor = color(255, 165, 0); // orange
    nextColor = color(255, 255, 0); // yellow
    currentGradient = 1;

 } else if (currentGradient === 1) {
  // change to pink-white gradient

    currentColor = color(255, 192, 203); // pink
    nextColor = color(255); // white
    currentGradient = 2;
 
 } else {
  // change to blue-purple gradient
    currentColor = color(0, 0, 255); // blue
    nextColor = color(148, 79, 240); // purple
    currentGradient = 0;
  
 }

}

