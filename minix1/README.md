[miniX1/circle.png](https://gitlab.com/ooreoz/aesthetic-programming/-/blob/main/miniX1/circle.png)

[runme](https://ooreoz.gitlab.io/aesthetic-programming/minix1/index.html)

[code](https://gitlab.com/ooreoz/aesthetic-programming/-/blob/main/minix1/minix1sketch.js)

my initial thoughts with the code is to make a spinning circle which changes color, which i quickly found out didn't make sense as you can't tell that the circle is spinning
i then decided that i should be making an open circle, which was more of an arch than a circle so that you could tell it's spinning around.

i also decided that the changing color should be in a gradient, to give the circle a more fun element. at first that turned into a rainbow gradient, however i then decided that i wanted it to change colors at the press of a button which is why i came up with 3 different gradients: blue-purple, orange-yellow and pink-white

my button in the middle was then made into a round button with a circus tent inside, which i felt represented the feel you got staring at the circle, sort of like a carnaval fun house

when pressing the button, the spinning circle changes colors to the next gradient, and it cycles through the 3 different ones i chose

+

coding this was very fun as i got to explore many different ways of playing with something as simple as a circle. i had a lot of different first drafts for this circle, however it seemed either too simple or too complicated, which made me explore different options which has then led me to my final product

i've used both youtube tutorials and the p5 resources to get through this assignment. it's something that took hours to function seemlessly, however, I'm proud of the end product seeing as it's doing exactly what i want it to do. that is, almost, as there are still a few issues, like the fact that it seems to take a while until the gradient works properly, as the colors simply flash between the two chosen colors instead of going through the spectrum for the first few seconds

i even toyed with the idea of making multiple circles spin around in different directions, which i did get kind of functioning, but seeing as I couldn't fully get it to work as it should, I chose to remove those extra circles in the end.

coding this has also taught me quite a lot of what's possible with p5 and I've stumbled upon many different options for it which will be worth exploring in the future.

-

my code starts off by declaring various variables which I used while programming what should happen on the web page. 

let angle = 0; - this sets the angle of my 'circle' to 0, since it's not actually a circle but an arch

let currentColor; - this speaks to the fact that my circle changes color, this variable is the current color of my circle , since it's a gradient which changes gradually

let nextColor; - this sets the next color that the circle turns to in the gradient, so for example, it would go from current color blue, to current color purple

let lerpedColor; - this variable is connected to the gradient, i am interpolating between 2 colors using the variable 'lerpColor' which chooses a color on the spectrum between those 2 colors

let button; - this speaks to the button inserted in the middle

let currentGradient = 0; - this variable is what changes between the gradients, there are 3 different gradients, which change at the click of the button. the value 0 is blue-purple

1 is orange-yellow
2 is pink-white

further down the code, i set up my canvas in the setup function
my canvas is 600 x 500 pixels to make sure everything is visible

i then set up my button and also the styling of the button where i used CSS

i then called a function for the button, which is given the argument 'Change Gradient', as that is what the button should do 

i also made adjustments to the button itself so that it's small and round using CSS, also changing the text to display a circus tent emoji

'button.mousePressed(changeGradient)' is using the event listener mousePressed to detect when the button is clicked so that the button can then change the gradient, as instructed in the initial function I defined earlier

next I defined which colors the initial gradient would be

the currentColor is set to the color code which represents blue

the nextColor is set to purple

and the lerpedColor, which is what makes the gradient, takes it back to blue


in the draw function, I first determine the background of the page, so that the code knows to run that first in order to erase the previous paths of the 'circle'


 lerpedColor = lerpColor(currentColor, nextColor, 0.05); // 0.05 is the speed

i then use the lerpedColor variable and give it the lerpColor function which should interpolate between the currentColor and the nextColor at 0.05, which translates to 5%, meaning it chooses a color that is 5%  away from the currentColor but doesn't go past the nextColor 

the stroke of the circle is also set to the same lerpedColor variable

then we get to the actual circle 

let circleCenterX = width / 2;
let circleCenterY = height / 2;

the circle is placed at half the width and half the height of the canvas, meaning it should be in the middle

then i had to make sure that the circle, wouldn't be fully closed, which is why it was made into a semi-circle with a 180 degree arch to it

this was written with the code:

let arcStartAngle = radians(angle);

let arcEndAngle = radians(angle + 180); 

(circleCenterX, circleCenterY, circleRadius * 2, circleRadius * 2, arcStartAngle, arcEndAngle);

at first, the start angle that was defined on the first line of the code, which was 0, had to be translated to radians, which is used instead of degrees as i found out the degree unit didn't work.

then, using:
let arcEndAngle = radians(angle + 180);

i determined that the semi circle should have an arc of 180 radians: radians(angle + 180) to make it bend into a semi circle shape

the last line of code: 

arc(circleCenterX, circleCenterY, circleRadius * 2, circleRadius * 2, arcStartAngle, arcEndAngle);

determines where the coordinates of the center of the arc are as well as giving the circle a width and a hight: 'circleRadius * 2' for both to make it perfectly round

then to make the circle spin, i used:
angle += 1+ 0.04; // Spinning speed

the += adds the value on the right side of the operator to the variable on the left side and assigns the result to the variable on the left side

this means i added a constant value of 1 to the angle so that it starts spinning
i also added it the value of 0.04, to give it more speed when spinning

i then made sure that the interpolated colors change smoothly, by counting the frames per second before it is allowed to change

by writing this code:
if (frameCount % 45 === 0)
{ currentColor = nextColor; nextColor = lerpedColor;

i made it count so that when the frame count is dividable by 45, meaning equal to 0, it would go from the current color to the next color, and then from the next color to the lerped color

then, i had to work on the buttons changing gradients

i used the changeGradient function to do this 
by using if, else if and else
i coded that if the currentGradient was at 0, which was the blue-purple gradient,
it should then change the currentColor to orange, the nextColor to yellow and the currentGradient counter to 1

this now makes the 0 from before 1, which has now been assigned the orange-yellow gradient

this then continues to the pink and white gradient, and then back to the blue and purple gradient, making the button cycle through the 3 different gradients in order

references:
https://p5js.org/reference/#/p5/circle

https://p5js.org/reference/#/p5/color

https://p5js.org/reference/#/p5/lerpColor

https://p5js.org/reference/#/p5/arc

https://p5js.org/reference/#/p5/radians

https://p5js.org/reference/#/p5/createButton





