Flowchart assignment MINIX9

![](minix9/flowchart.png)

the flowchart has been done to show the process of my minix6 assignment which shows a small game involving a ghost trying to catch all the lost souls

[see game here](https://gitlab.com/ooreoz/aesthetic-programming/-/tree/main/minix6?ref_type=heads)


the group flowchart assignment can be found here:
[figma flowchart page](https://www.figma.com/board/XDyn1jOk5JWOMyZY6FfNGo/miniX09-f%C3%A6lles?node-id=0-1&t=lTlzfv5svqw3quhk-0)